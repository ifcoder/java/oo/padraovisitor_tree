package com.ifet.padraovisitor_tree.patternVisitor.interfaces;

import com.ifet.padraovisitor_tree.classes.ArvoreBinaria;
import classes.No;

/**
 *
 * @author jose
 */
public interface IVisitor {
    
    void visit(ArvoreBinaria tree);
    void visit(No no);
}
