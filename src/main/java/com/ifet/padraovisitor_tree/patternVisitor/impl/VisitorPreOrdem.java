package com.ifet.padraovisitor_tree.patternVisitor.impl;

import com.ifet.padraovisitor_tree.classes.ArvoreBinaria;
import classes.No;
import com.ifet.padraovisitor_tree.patternVisitor.interfaces.IVisitor;

/**
 *
 * @author jose
 */
public class VisitorPreOrdem implements IVisitor{
    @Override
    public void visit(ArvoreBinaria tree) {
        visit(tree.getRoot());
    }
    
    @Override
    public void visit(No no) {                
        if(no != null)
           System.out.print(no.getItem()+", ");
        
        if(no.getSubEsquerda() != null)
            visit(no.getSubEsquerda());
                
        if(no.getSubDireita() != null)
            visit(no.getSubDireita());        
    }
    
}
