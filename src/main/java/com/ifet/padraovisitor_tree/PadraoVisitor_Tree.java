
package com.ifet.padraovisitor_tree;

import com.ifet.padraovisitor_tree.classes.ArvoreBinaria;
import com.ifet.padraovisitor_tree.patternVisitor.impl.VisitorEmOrdem;
import com.ifet.padraovisitor_tree.patternVisitor.impl.VisitorPreOrdem;

/**
 *
 * @author jose
 */
public class PadraoVisitor_Tree {

    public static void main(String[] args) {
        ArvoreBinaria tree = new ArvoreBinaria();
        tree.insert(4);
        tree.insert(2);
        tree.insert(8);
        tree.insert(7);
        System.out.print("Pre-ordem:");
        tree.accept(new VisitorPreOrdem());
        
        System.out.print("\nEm-ordem:");
        tree.accept(new VisitorEmOrdem());
              
    }
}
