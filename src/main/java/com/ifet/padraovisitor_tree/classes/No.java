/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package classes;

/**
 *
 * @author jose
 */
public class No {
    private No subEsquerda;    
    private double item;
    private No subDireita;

    public No() {
        this.subEsquerda = null;
        this.item = -1;
        this.subDireita = null;
    }

    public No(No subEsquerda, double item, No subDireita) {
        this.subEsquerda = subEsquerda;
        this.item = item;
        this.subDireita = subDireita;
    }

    /**
     * @return the subEsquerda
     */
    public No getSubEsquerda() {
        return subEsquerda;
    }

    /**
     * @param subEsquerda the subEsquerda to set
     */
    public void setSubEsquerda(No subEsquerda) {
        this.subEsquerda = subEsquerda;
    }

    /**
     * @return the item
     */
    public double getItem() {
        return item;
    }

    /**
     * @param item the item to set
     */
    public void setItem(double item) {
        this.item = item;
    }

    /**
     * @return the subDireita
     */
    public No getSubDireita() {
        return subDireita;
    }

    /**
     * @param subDireita the subDireita to set
     */
    public void setSubDireita(No subDireita) {
        this.subDireita = subDireita;
    }
    
    
}
