
package com.ifet.padraovisitor_tree.classes;

import classes.No;
import com.ifet.padraovisitor_tree.patternVisitor.interfaces.IVisitor;

/**
 *
 * @author jose
 */
public class ArvoreBinaria {

    /**
     * @return the root
     */
    public No getRoot() {
        return root;
    }

    private No root;

    public ArvoreBinaria() {
        this.root = null;
    }

    public void accept(IVisitor v){       
        v.visit(this);    
    }

    
    
    public void insert(double valor) {
        if (getRoot() == null) {
            root = new No();
            getRoot().setItem(valor);
        } else {
            insert(valor, getRoot());
        }
    }

    private void insert(double valor, No node) {
        if (valor < node.getItem()) {
            if (node.getSubEsquerda() == null) {
                No novo = new No();
                novo.setItem(valor);
                node.setSubEsquerda(novo);
            } else {
                insert(valor, node.getSubEsquerda());
            }
        } else if (valor > node.getItem()) {
            if (node.getSubDireita() == null) {
                No novo = new No();
                novo.setItem(valor);
                node.setSubDireita(novo);
            } else {
                insert(valor, node.getSubDireita());
            }
        }
    }

}
