# padraoVisitor_Tree


- O **Padrão Visitor** é um padrão de design de software que permite separar a `lógica` de um objeto de sua `estrutura`.
- Ele é útil quando você tem uma estrutura de objetos complexa e deseja executar operações diferentes em cada objeto sem modificar a própria estrutura.
- Em termos simples, o **Padrão Visitor** permite que você adicione novas operações aos objetos existentes sem alterar a própria estrutura do objeto.

- O **Padrão Visitor** consiste em duas partes principais: o `Element` e o `Visitor`.
    - O **Element**:
        - é a estrutura de objeto complexa que você deseja manipular
        - é a classe que vai receber o `visitor`
    - O **Visitor**:
        - Anota o método/comportamento `visit()`
        - é o visitante que adentrará a `element` e fará a ação
        - é a classe que contém a lógica que será executada em cada nó da estrutura.

- No codigo temos
    - **Element** → `ArvoreBinaria`
        - Tem o método `accept(Visitor v)`
        - Vai receber as classes visitantes que vão operar o objeto
    - **Visitor** → interface `Visitor`
        - Simplesmente faz a abstração do conceito
        - Deve ser implementada por alguem
        - É a base do Open-close principle, quem implementar a interface **************visitor************** está apta entrar no ************accept************
    - **ConcretVisitor** → `ExibirPreOrdem`, `ExibirEmOrdem`
        - É o visitante de fato
        - É a classe onde terá a implementação do método `visit()`
        - A cada novo método de “passeio” na arvore binária criamos uma nova classe ****************************concretVisitor****************************
